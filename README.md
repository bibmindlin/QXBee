QXbee
======

A Qt library for use with XBee DigiMesh modems, initially based on [Anthony's QtXbee](https://github.com/anthonypocock/QtXbee).

This library works with XBee in API mode only.

