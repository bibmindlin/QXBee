#include <QCoreApplication>
#include <QObject>
#include <QTimer>
#include <QtSerialPort/QSerialPort>
#include "QXBee.h"

class Task : public QObject
{
    Q_OBJECT
public:
    Task(QObject *parent = nullptr) : QObject(parent) {}

public slots:
    void run()
    {

        // Setup serial port
        QSerialPort serial;
        serial.setPortName("ttyUSB0");
        serial.setBaudRate(QSerialPort::Baud9600);
        serial.setDataBits(QSerialPort::Data8);
        serial.setParity(QSerialPort::NoParity);
        serial.setStopBits(QSerialPort::OneStop);
        serial.setFlowControl(QSerialPort::NoFlowControl);
        serial.open(QIODevice::ReadWrite);

        // Create QXbee object with API mode 2
        QXBee xb(serial, 2);

        // Send text to all XBee modules
        qDebug("Sending broadcast message: \"Hello World!\"");
        xb.broadcast("Hello World!");

        emit finished();
    }

signals:
    void finished();
};

#include "main.moc"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Task *task = new Task(&a);
    QObject::connect(task, SIGNAL(finished()), &a, SLOT(quit()));
    QTimer::singleShot(0, task, SLOT(run()));

    return a.exec();
}
