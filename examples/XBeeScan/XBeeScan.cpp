#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QDebug>
#include <QSettings>
#include <QTextStream>

#include "XBeeScan.h"

XBeeScan::XBeeScan(QObject *parent) : QObject(parent)
{
        QSettings settings("xbeescan.cfg", QSettings::IniFormat );
		if (settings.status() != QSettings::NoError) {
            qFatal("XBeeScan: Error reading configurarion file.");
		}
        settings.beginGroup("Device");
		QString port = settings.value("Serial Port").toString();
        uint8_t APImode = uint8_t(settings.value("API Mode").toUInt());
        settings.endGroup();

        serial = new QSerialPort(this);
        serial->setPortName(port);
        serial->setBaudRate(QSerialPort::Baud9600);
        serial->setDataBits(QSerialPort::Data8);
        serial->setParity(QSerialPort::NoParity);
        serial->setStopBits(QSerialPort::OneStop);
        serial->setFlowControl(QSerialPort::NoFlowControl);
        serial->open(QIODevice::ReadWrite);

        if(serial->isOpen())
        {
            qDebug("XBeeScan: Serial port opened for read and write.");
            xb = new QXBee(*serial, APImode);
            QObject::connect(xb, &QXBee::dataReceived, this, &XBeeScan::displayData);

            ATCommand scanNetwork;
            scanNetwork.setFrameID(15);
            scanNetwork.setATCommand("ND");
            xb->send(scanNetwork);
        }
}

 void XBeeScan::displayData(XBeePacket& packet){
	 static int count = 1;
	 int idx;

	 switch (packet) {
		 case XBeePacket::ATCommandResponse:
		 {
             if (static_cast<ATCommandResponse &>(packet).getFrameID() == 15) {
                 QByteArray data = static_cast<ATCommandResponse &>(packet).getCommandData();
                 idx = data.indexOf(char(0x00), 10);
                 qDebug().noquote() << "XBeeScan: " << QString("%1").arg(count++, 3, 10, QChar('0')) << "Address:" << data.mid(2, 8).toHex() << "Name:" << data.mid(10,idx-10);
             }
		 }
		 break;
		 default:
		 {
             qDebug() << "XBeeScan: Received Data: " << packet.getFrameData().toHex();
		 }
	 }
 }

 XBeeScan::~XBeeScan() {
	 delete xb;
     if(serial->isOpen())
     {
         serial->close();
         qDebug() << "XBeeScan: Serial port closed successfully.";
     }
     delete serial;
 }
