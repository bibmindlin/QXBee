#include <QCoreApplication>
#include <signal.h>

#include "XBeeScan.h"

void handler(int) { QCoreApplication::quit(); }


int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	signal(SIGINT, handler);

	XBeeScan run;

	return a.exec();
}
