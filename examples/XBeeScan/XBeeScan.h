#ifndef XBEESCAN_H
#define XBEESCAN_H

#include <QObject>

#include "QXBee.h"

class QSerialPort;

class XBeeScan : public QObject
{
	Q_OBJECT
	QXBee *xb;
    QSerialPort *serial;
public:
	explicit XBeeScan(QObject *parent = 0);
	~XBeeScan();
public slots:
	void displayData(XBeePacket&);

};

#endif // XBEESCAN_H
