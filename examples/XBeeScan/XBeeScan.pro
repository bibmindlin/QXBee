QT += core
QT += serialport
QT -= gui

CONFIG += c++11

TARGET = XBeeScan
OBJECTS_DIR = ../../build/XBeeScan
MOC_DIR = ../../build/XBeeScan

CONFIG += console
CONFIG -= app_bundle

greaterThan(QT_MAJOR_VERSION, 4): CONFIG += serialport

TEMPLATE = app

include($$PWD/../../src/QXBee/QXBee.pri)

INCLUDEPATH += $$PWD/../../src/QXBee
INCLUDEPATH += $$PWD/../../src/QXBee/XBeePacket
DEPENDPATH += $$PWD/../../src/QXBee

SOURCES += \
	$$PWD/main.cpp \
	$$PWD/XBeeScan.cpp

HEADERS += \
	$$PWD/XBeeScan.h
