TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS = \
	src/QXBee \
	src/check \
	examples

src/check.depends = src/QXBee
examples.depends = src/QXBee
