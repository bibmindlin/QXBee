QT       += core
QT       -= gui
greaterThan(QT_MAJOR_VERSION, 4): QT += serialport

TARGET = ../../QXBee
OBJECTS_DIR = ../../build
MOC_DIR = ../../build

CONFIG += staticlib
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11
greaterThan(QT_MAJOR_VERSION, 4): CONFIG += serialport

TEMPLATE = lib

INCLUDEPATH += $$PWD
INCLUDEPATH += $$PWD/XBeePacket

greaterThan(QT_MAJOR_VERSION, 4){

LIBS += \
	-L/usr/lib \
	-L/usr/local/lib \

} else {

LIBS += \
	-L/usr/lib \
	-lQtSerialPort \
}

unix {
    target.path = /usr/local/lib
    INSTALLS += target
}

SOURCES += \
	$$PWD/QXBee.cpp \
	$$PWD/XBeePacket/ATCommand.cpp \
	$$PWD/XBeePacket/ATCommandRemote.cpp \
	$$PWD/XBeePacket/ATCommandResponse.cpp \
	$$PWD/XBeePacket/ATCommandResponseRemote.cpp \
	$$PWD/XBeePacket/ModemStatus.cpp \
	$$PWD/XBeePacket/NodeIdentificationIndicator.cpp \
	$$PWD/XBeePacket/RXIndicator.cpp \
	$$PWD/XBeePacket/RXIndicatorExplicit.cpp \
	$$PWD/XBeePacket/TransmitStatus.cpp \
	$$PWD/XBeePacket/TXRequest.cpp \
	$$PWD/XBeePacket/TXRequestExplicit.cpp \
	XBeePacket/XBeePacket.cpp

HEADERS += \
	$$PWD/QXBee.h \
	$$PWD/XBeePacket/ATCommand.h \
	$$PWD/XBeePacket/ATCommandQueue.h \
	$$PWD/XBeePacket/ATCommandRemote.h \
	$$PWD/XBeePacket/ATCommandResponse.h \
	$$PWD/XBeePacket/ATCommandResponseRemote.h \
	$$PWD/XBeePacket/ModemStatus.h \
	$$PWD/XBeePacket/NodeIdentificationIndicator.h \
	$$PWD/XBeePacket/RXIndicator.h \
	$$PWD/XBeePacket/RXIndicatorExplicit.h \
	$$PWD/XBeePacket/TransmitStatus.h \
	$$PWD/XBeePacket/TXRequest.h \
	$$PWD/XBeePacket/TXRequestExplicit.h \
	$$PWD/XBeePacket/XBeePacket.h
