#include <QDebug>
#include <QIODevice>

#include "QXBee.h"

QXBee::QXBee(QIODevice& port, uint8_t APIMode){
    ioDevice = &port;
    QByteArray data;
    this->APIMode = APIMode;

    if (ioDevice->openMode() == QIODevice::ReadWrite && ioDevice->isOpen())
    {
        connect(ioDevice, SIGNAL(readyRead()), SLOT(readData()));
        qDebug() << "QXBee: Connected and ready.";
    } else {
        qFatal("QXBee: Error, no connection available.");
    }
}

QXBee::~QXBee()
{
}

void QXBee::send(XBeePacket &packet)
{
    union {
        uint16_t value;
        struct {
            uint8_t loByte;
            uint8_t hiByte;
        };
    } frameLength;
    uint8_t frameChecksum = 0;
    QByteArray frame, data;

    data = packet.getFrameData();
    frameLength.value = uint8_t(data.length());

    // Calculate checksum
    for (int i = 0; i < data.length(); i++) frameChecksum += data[i];
    frameChecksum = 0xFF - frameChecksum;

    // Assemble frame
    frame += startDelimiter;
    if (APIMode == 2) {
        frame += escape(frameLength.hiByte);
        frame += escape(frameLength.loByte);
        frame += escape(data);
        frame += escape(frameChecksum);
    } else {
        frame += char(frameLength.hiByte);
        frame += char(frameLength.loByte);
        frame += data;
        frame += char(frameChecksum);
    }

    // Transmit frame
    if(ioDevice->isOpen())
	{
        qDebug() << "QXBee: Transmit: " << frame.toHex();
        ioDevice->write(frame);
	}
	else
	{
        qCritical() << "QXBee: IO device not acessible.";
    }
}

void QXBee::broadcast(QString data)
{
	TXRequest packet;
    packet.setTransmitingData(data.toLocal8Bit());
    send(packet);
}

void QXBee::unicast(QByteArray address, QString data){
	TXRequest packet;
	packet.setDestinationAddress64(address);
    packet.setTransmitingData(data.toLocal8Bit());
    send(packet);
}

void QXBee::readData()
{
    int frameIndex;
	union {
        uint16_t value;
        struct {
            uint8_t loByte;
            uint8_t hiByte;
        };
	} frameLength;

    // Read data and unescape if necessary.
    switch (APIMode) {
        case 1:
            frameBuffer += ioDevice->readAll();
			break;
		case 2:
            rawFrameBuffer += ioDevice->readAll();
            for (frameIndex = 0; frameIndex < rawFrameBuffer.size(); frameIndex++) {
                if (uint8_t(rawFrameBuffer[frameIndex]) != escapeCharacter)
                    frameBuffer += rawFrameBuffer[frameIndex];
                else if (rawFrameBuffer.length() > frameIndex+1)
                    frameBuffer += rawFrameBuffer[++frameIndex]^0x20;
				else break;
			}
            // Remove processed data from raw buffer.
            rawFrameBuffer.remove(0, frameIndex);
	}

    // Process frames
    while (frameBuffer.size() > 0) {

        // Remove junk bytes.
        for (frameIndex = 0; frameIndex < frameBuffer.size(); frameIndex++)
            if (uint8_t(frameBuffer[frameIndex]) == startDelimiter) break;
        if (frameIndex) frameBuffer.remove(0, frameIndex);

        // Stop if we don't have the minimum frame size.
        if (frameBuffer.size() < 6) return;

		// Get frame length
        frameLength.hiByte = uint8_t(frameBuffer[1]);
        frameLength.loByte = uint8_t(frameBuffer[2]);

        // Stop if we don't have a full frame.
        if(frameBuffer.size()-4 < frameLength.value) return;

		// Verify frame checksum
        uint8_t frameChecksum = 0;
        for (frameIndex = 3; frameIndex < frameLength.value+3; frameIndex++) frameChecksum += frameBuffer[frameIndex];
        frameChecksum = 0xFF - frameChecksum;

		// Save frame
        if (frameChecksum == uint8_t(frameBuffer[frameLength.value+3])) {
            processPacket(frameBuffer.mid(3,frameLength.value));
        } else qDebug() << "QXBee: Frame checksum Error: " << frameBuffer.mid(3,frameLength.value).toHex();

        // Remove processed frame from buffer.
        frameBuffer.remove(0, frameLength.value+4);
	}
}

void QXBee::processPacket(QByteArray frame){
    switch (uint8_t(frame[0])) {
	case XBeePacket::ATCommandResponse:{
		ATCommandResponse packet;
		packet.setFrameData(frame);
		emit dataReceived(packet);
		break;
	}
	case XBeePacket::ModemStatus:{
		ModemStatus packet;
		packet.setFrameData(frame);
		emit dataReceived(packet);
		break;
	}
	case XBeePacket::TransmitStatus:{
		TransmitStatus packet;
		packet.setFrameData(frame);
		emit dataReceived(packet);
		break;
	}
	case XBeePacket::RXIndicator:{
		RXIndicator packet;
		packet.setFrameData(frame);
		emit dataReceived(packet);
		break;
	}
	case XBeePacket::RXIndicatorExplicit:{
		RXIndicatorExplicit packet;
		packet.setFrameData(frame);
		emit dataReceived(packet);
		break;
	}
	case XBeePacket::NodeIdentificationIndicator:{
		NodeIdentificationIndicator packet;
		packet.setFrameData(frame);
		emit dataReceived(packet);
		break;
	}
	case XBeePacket::ATCommandResponseRemote:{
		ATCommandResponseRemote packet;
		packet.setFrameData(frame);
		emit dataReceived(packet);
		break;
	}
	default:
        qDebug() << "QXBee: Unknown Packet: " << frame.toHex();
	}
}

QByteArray QXBee::escape(QByteArray data) {
    QByteArray escapedData;

    for (int i = 0; i < data.length(); i++) {
        if (uint8_t(data[i]) == startDelimiter || uint8_t(data[i]) == escapeCharacter || uint8_t(data[i]) == XONCharacter || uint8_t(data[i]) == XOFFCharacter) {
            escapedData.append(escapeCharacter);
            escapedData.append(data[i]^0x20);
        } else {
            escapedData.append(data[i]);
        }
    }
    return escapedData;
}

QByteArray QXBee::escape(uint8_t data) {
    QByteArray escapedData;
    escapedData += char(data);
    return escape(escapedData);
}
