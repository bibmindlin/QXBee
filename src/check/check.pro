QT += core testlib
QT -= gui

greaterThan(QT_MAJOR_VERSION, 4): QT += serialport

TARGET = ../../build/check/check
OBJECTS_DIR = ../../build/check
MOC_DIR = ../../build/check

CONFIG -= app_bundle
CONFIG += console testcase
CONFIG += c++11
greaterThan(QT_MAJOR_VERSION, 4): CONFIG += serialport

TEMPLATE = app

INCLUDEPATH += $$PWD/../../src/QXBee
INCLUDEPATH += $$PWD/../../src/QXBee/XBeePacket
DEPENDPATH += $$PWD/../../src/QXBee

greaterThan(QT_MAJOR_VERSION, 4){

LIBS += \
    -L/usr/lib \
    -L/usr/local/lib \

} else {

LIBS += \
    -L/usr/lib \
    -lQtSerialPort \
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../release/ -lQXBee
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../debug/ -lQXBee
else:unix: LIBS += -L$$OUT_PWD/../../ -lQXBee
win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../release/libQXBee.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../debug/libQXBee.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../release/QXBee.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../debug/QXBee.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../libQXBee.a

SOURCES +=  \
    $$PWD/testATCommand.cpp \
    $$PWD/testATCommandRemote.cpp \
    $$PWD/testATCommandResponse.cpp \
    $$PWD/testATCommandQueue.cpp \
    $$PWD/testATCommandResponseRemote.cpp \
    $$PWD/testModemStatus.cpp \
    $$PWD/testNodeIdentificationIndicator.cpp \
    $$PWD/testRXIndicator.cpp \
    $$PWD/testRXIndicatorExplicit.cpp \
    $$PWD/testTransmitStatus.cpp \
    $$PWD/testTXRequest.cpp \
    $$PWD/testTXRequestExplicit.cpp \
    $$PWD/main.cpp

HEADERS +=  \
    $$PWD/Autotest.h \
    $$PWD/testATCommand.h \
    $$PWD/testATCommandRemote.h \
    $$PWD/testATCommandResponse.h \
    $$PWD/testATCommandQueue.h \
    $$PWD/testATCommandResponseRemote.h \
    $$PWD/testModemStatus.h \
    $$PWD/testNodeIdentificationIndicator.h \
    $$PWD/testRXIndicator.h \
    $$PWD/testRXIndicatorExplicit.h \
    $$PWD/testTransmitStatus.h \
    $$PWD/testTXRequest.h \
    $$PWD/testTXRequestExplicit.h
