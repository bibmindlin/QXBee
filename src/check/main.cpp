#include "AutoTest.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    int failures = AutoTest::run(argc, argv);
    if (failures == 0)
    {
	qWarning() << "\033[1;32mALL TESTS PASSED\033[0m\n";
    }
    else
    {
	qWarning() << "\033[1;31m" << failures << " TESTS FAILED!\033[0m\n";
    }
    return failures;
}
